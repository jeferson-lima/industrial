.PHONY: help prepare-dev gen

DATA_DIR=pandoc
PDF_ENGINE=lualatex
SRC=content
BUILD=public
MDFILES=$(wildcard content/*.md)
DATE_COVER=$(shell date "+%d %B %Y")
PDFS := $(patsubst $(SRC)/%.md,$(BUILD)/%.pdf,$(MDFILES))

SOURCE_FORMAT=	markdown_strict$\
		+backtick_code_blocks$\
		+auto_identifiers$\
		+strikeout$\
		+yaml_metadata_block$\
		+implicit_figures$\
		+all_symbols_escapable$\
		+link_attributes$\
		+smart$\
		+fenced_divs$\
		+tex_math_dollars$\
		+task_lists$\
		
ARGS= -s\
      --dpi=300\
      --slide-level 2\
      --toc --listings\
      --shift-heading-level=0\
      --data-dir=${DATA_DIR}\
      --template default_mod.latex\
      --pdf-engine ${PDF_ENGINE}\
      -M date='${DATE_COVER}'\
      -V classoption:aspectratio=169

.DEFAULT: all

all: $(PDFS)
	                        
$(BUILD)/%.pdf: $(SRC)/%.md
	@echo "$(notdir $@) building..."
	@pandoc $(ARGS) -t beamer $< -o $@

md:
	@pandoc README.md -f markdown -t html -s -o public/README.html

clean:
	@$(RM) public/*.pdf
