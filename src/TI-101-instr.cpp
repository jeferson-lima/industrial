/* ----------------------------------------------------------------------------
 * Copyright 2023, Jeferson Lima
 * All Rights Reserved
 * See LICENSE for the license information
 * -------------------------------------------------------------------------- */

/**
 *  @file   tt101.cpp
 *  @author Jeferson Lima
 *  @brief  Example to simulate a Temperature Transmissor
 *  @date   July 25, 2023
 **/

#include <Adafruit_LiquidCrystal.h>
#define Ent_Transmissao A0

Adafruit_LiquidCrystal lcd_1(0);

void setup()
{
  lcd_1.begin(16, 2);
  lcd_1.setCursor(0, 0);
  lcd_1.print("Ind. Temperatura:");
}

void loop()
{
  int Temp = analogRead(Ent_Transmissao);
  Temp = map(Temp, 205, 1023, -40, 125);
  lcd_1.setCursor(0, 1);
  lcd_1.print(Temp);
  lcd_1.print(" .C");
  delay(500); // Wait for 500 millisecond(s)
}
