/* ----------------------------------------------------------------------------
 * Copyright 2023, Jeferson Lima
 * All Rights Reserved
 * See LICENSE for the license information
 * -------------------------------------------------------------------------- */

/**
 *  @file   tt101.cpp
 *  @author Jeferson Lima
 *  @brief  Example to simulate a Temperature Transmissor
 *  @date   July 25, 2023
 **/
#define Sensor A0
#define SaidaPWM 5

void setup()
{
  pinMode(SaidaPWM, OUTPUT);
  Serial.begin(9600);
}

void loop()
{
  int Temp = analogRead(Sensor);
  Serial.print("Valor sensor: ");
  Serial.println(Temp);
  Temp = map(Temp, 20, 358, 50, 255);
  analogWrite(SaidaPWM,Temp);
  delay(1000); // Wait for 1000 millisecond(s)
}
