---
title: "Automação Industrial"
subtitle: "Contexto Histórico"
author: "Jeferson Lima"
institute: "Federal Technological University of Paraná (UTFPR)"
topic: "industrial-automation-Introduction"
theme: "Frankfurt"
colortheme: "beaver"
fonttheme: "professionalfonts"
mainfont: "Hack Nerd Font"
fontsize: 10pt
urlcolor: blue
linkstyle: bold
aspectratio: 169
date:
section-titles: false
toc: true
---

# Information

### Licence
> This work is licensed under a Creative Commons “AttributionNonCommercial-ShareAlike 4.0 International” license.

### Links
* [UTFPR Moodle](https://moodle.utfpr.edu.br/course/view.php?id=7183)

* password: ******

# A Revolução Industrial

- A Primeira Revolução Industrial

> A Primeira Revolução Industrial estabeleceu um marco transitório de uma sociedade, predominantemente, agrícola e organizada na produção de bens de consumo de forma artesanal, para uma sociedade, industrialmente. 

---

- Tendência da Ocupação da Mão de Obra

> Segundo Jean Fourastié os setores de atividades podem ser classificados em três grandes categorias: **primárias** - atividades do tipo agrícola; **secundárias** - atividades industriais; **terciárias** - prestação de serviços, como o ensino, as artes, consultorias, entre outros.

---

::: columns
:::: {.column width=40%}

- Silvera, et al. (1999), 

::::
:::: {.column width=60%}
![Gráfico de tendência de ocupação de mão de obra](content/figures/da39a3ee5e6b4b0d3255bfef95601890afd80709)
::::
:::

---

::: columns
:::: {.column width=40%}

- No contexto atual
> a busca a LIGHTS-OUT Manufacturing!

::::
:::: {.column width=60%}
![conceito de manufatura light-out](https://avozdaindustria.com.br/sites/avozdaindustria.com/files/styles/article_featured_standard/public/lights%20out%20manufacturing%20manufatura%20apagada.png?itok=cQC5mMks)

::::
:::

# Evolução do Controle Industrial


## Controle Numérico

> O princípio de funcionamento de um comando numérico consiste no recebimento de um programa pela unidade de entrada, na leitura, interpretação, armazenamento e sua execução. A extensão lógica do controle numérico, com o advento dos microprocessadores, foi o controle numérico _computadorizado_ - CNC. 

## Controladores Lógicos

::: columns
:::: {.column width=40%}

> A evolução dos sistemas de controle definem os marcos da revolução industrial.
\

- **Controlador lógico programável (CLP)**

> Os CLPs são hoje a tecnologia de controle de processos industriais mais amplamente utilizada.

::::
:::: {.column width=60%}
![Evolução dos sistemas de controle desde o final do século XIX](content/figures/7ff057c99a04f6eadff9157542901d07db8ea4b2){.column width=90%}
::::
:::

---

::: columns
:::: {.column width=40%}

 - **Controladores Lógico Programável (CLP)**

> Um CLP é um tipo de computador industrial que pode ser programado para executar funções de controle

::::
:::: {.column width=60%}
![Controlador Lógico Programável](content/figures/ad06bc054a859a6189167704eb73520b853f3ae9){.column width=90%}
::::
:::


# A Industrial 4.0

- [x] **A Primeira Revolução Industrial**
    > Em torno de 1750, começou a ficar mais acentuado o surgimento da equi-pamentos mecanizados.

---

- [x] **A Segunda Revolução Industrial**
    > No final do século XIX, a partir de 1860, o uso de energia elétrica impulsionou a modernização de máquinas e equipamentos industriais.

---

- [x] **A Terceira Revolução Industrial**
    > Também conhecida como Revolução Digital e Revolução Técnico-Científica, a Terceira Revolução Industrial foi marcada pelo advento dos semicondutores, particularmente o “Transistor”, que proporcionou a modernização dos computadores e demais equipamentos elétricos e eletromecânicos.

---

- [ ] **A Quarta Revolução Industrial (loading...)**
    > A Indústria 4.0, a Quarta Revolução Industrial, é a era da interação digital da indústria, caracterizando o conceito de Fábrica Inteligente, do inglês _Smart Factory_.

## Cyber-physical Systems

## Evolução da Robótica

> Continuação [link](https://jeferson.aulas.gitlab.io/mobile-robotics/modern-robots-aula.pdf)


# Referências

1. BERGER, Hans.**Automating with STEP 7 in STL and SCL: programmable controllers : SIMATIC S7-300/400**. Erlangen: MCD Verlag, 2000. 436 p. : ISBN 3-89578-140-1
1. GEORGINI, Marcelo. **Automação aplicada: descrição e implementação de sistemas seqüenciais com PLCs**. 9. ed. São Paulo, SP: Érica, 2007. 236 p. ISBN 9788571947245.
1. SILVEIRA, Paulo Rogério da; SANTOS, Winderson E. dos. Automação e controle discreto. São Paulo, SP: Érica, 1999. 229 p. ISBN 8571945918.
1. Franchi, Claiton Moro, and Valter Luís Arlindo de CAMARGO. Controladores Lógicos Programáveis Sistemas Discretos. Saraiva Educação SA, 2008.
