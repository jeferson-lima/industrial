---
title: "Numerical System"
subtitle: "and Algorithms"
author: "Jeferson Lima"
institute:  
      - Department of Electronics (DAELE) 
      - Federal Technological University of Paraná (UTFPR) 
topic: "industrial-automation-NunSystem"
theme: "Frankfurt"
colortheme: "beaver"
fonttheme: "professionalfonts"
mainfont: "Hack Nerd Font"
fontsize: 10pt
urlcolor: blue
linkstyle: bold
aspectratio: 169
date:
section-titles: false
toc: true
header-includes: |
    \usepackage{tikz}
---


# Information

### Licence
This work is licensed under a Creative Commons “AttributionNonCommercial-ShareAlike 4.0 International” license.

### Links
* [UTFPR Moodle](https://moodle.utfpr.edu.br/course/view.php?id=7183)

* password: ******

# Decimal System

- Knowledge of different number systems and digital codes is essential when working with PLCs or most types of digital computers.

---

- The decimal system is the system commonly used in society, but ...

### Decimal System

- We can ask the question: Why did human beings choose decimal systems?

---

-  The decimal system consists of 10 symbols or numerals, as shown below:

$$ \text{Base}_{10} \rightarrow 0,1,2,3,4,5,6,7,8,9. $$ 

- Example(1) the number 293 is decomposed as:

$$
\begin{split}
  293 &= 2\ \text{hundreds}+9\ \text{tens}+3\ \text{units}\\
  &= 2\cdot 10^2+9\cdot 10^1+3\cdot 10^0.
\end{split}
$$

---

- The positional numbering system can also be used with other bases. Let's look at the following definition.

### Definition (1)
Given a natural number $b>1$ and the set of symbols $\{\pm,0, 1, 2, \cdots, b-1\}$, the sequence of symbols:

$$ \left(d_nd_{n-1} \cdots d_1d_0,d_{-1}d_{-2} \cdots \right)_b $$

represents the positive number

$$
d_n\cdot b^{n-1} + d_{n-1}\cdot b^{n-2} + \cdots + d_1\cdot b^0 + d_{0}\cdot b^{-1}+d_{-1}\cdot b^{-2} + \cdots
$$

---

- in other words,

$$
\begin{split}
 293_{10}, \text{com } n=3 \rightarrow & 2 \cdot 10^{3-1} + 9 \cdot 10^{3-2} + 3 \cdot 10^{3-3} \\
\rightarrow & 300 +  90 +  3
\end{split}
$$

---

::: columns
:::: {.column width=80%}

> **Algorithm**: Sequence of executable actions to obtain a solution to a
certain type of problem.

```c
// pseudo-codigo
#define MAX_BASE 9
#define N 2
...

for(int n = 0, n < (N-1), n++)
{
    if (display[n] > MAX_BASE)
    {
        display[n] = 0;
        display[n+1] ++;
    }
}
display[0] ++;
...
```

::::
:::: {.column width=20%}

 ![](content/figures/3f0743ffbd1407f98b1207260e25134129da9b51){width=100%}



|0x|1x|2x|
|---|---|---|
|00 |10 |20 | 
|01 |11 |21 | 
|02 |12 |22 | 
|03 |13 |23 | 
|04 |14 |24 | 
|05 |15 |25 | 
|06 |16 |26 | 
|07 |17 |27 | 
|08 |18 |28 | 
|09 |19 |29 | 

::::
:::

# Binary System

- The binary system uses the number 2 as its base, and the only digits allowed are 0 and 1.

. . .

- With digital circuits, it is easy to distinguish between two voltage levels (i.e. +5 V and 0 V), which can be related to the binary digits 1 and 0, so this system can be easily applied to PLCs and computer systems.

---

::: columns
:::: {.column width=55%}

- PLC memory is organized using bytes, single words or double words. 
\

. . .

> The smallest unit in the binary system is the Binary Digit (Bit).
\

- **Nibble** \  4 Bits

- **Byte** \ \ \  8 Bits

- **Word** \ \ \  16 Bits

. . .

:::
:::: {.column width=45%}

![](content/figures/ec4a07727f6a807800e6045fb697b7f94c34a902)


![](content/figures/041221c1ca77f0c86d5ff028fba43bcdd2690926)

:::
::::

---

- The binary system is made up of 2 symbols or numerals, as shown below:

$$ \text{Base}_{2} \rightarrow 0,1. $$ 

. . .

- Example(2) the number 101 is decomposed as:

$$
\begin{split}
  101_{10} &= 1\ \text{hundreds}+0\ \text{tens}+1\ \text{units}\\
  &= 1\cdot 10^2+0\cdot 10^1+1\cdot 10^0. \\
  &= 101_{10}.
\end{split}
$$

---

- Or, considering definition (1):

$$
\begin{split}
 101_{2}, \text{com } n=3 \rightarrow & 1 \cdot 2^{3-1} + 0 \cdot 2^{3-2} + 1 \cdot 2^{3-3} \\
\rightarrow & 4 + 0 + 1\\
\rightarrow & 5_{10}
\end{split}
$$

. . .

### Exercise

Convert from binary to decimal.

1. $1010_{2}$
1. $1111_{2}$
1. $0010_{2}$

---

::: columns
:::: {.column width=70%}

- Algorithm for binary to decimal conversion:

```c
// pseudo-codigo
...
int convert(long n)
{
    int dec = 0;
    int i = 0, rem;

    while (n!=0)
    {
        rem = n % 10;
        n /= 10;
        dec += rem * pow(2, i);
        ++i;
    }
return dec;
}
...
```

::::
:::: {.column width=30%}


|Binário    |Decimal  | 
|---        | ---     |
|0000       | 00      |
|0001       | 01      |
|0010       | 02      |
|0011       | 03      |
|0100       | 04      |
|0101       | 05      |
|0110       | 06      |
|0111       | 07      |
|1000       | 08      |
|1001       | 09      |
|1010       | 10      |
|1011       | 11      |
|1100       | 12      |
|1101       | 13      |
|1110       | 14      |
|1111       | 15      |


::::
:::

---


- Converting from decimal to binary.


$$ 637_{10} \rightarrow ?_{2} $$


. . .

\centering
![](content/figures/230ac5a5143ef06ea4eb6b7a71d5e15c81a9e69e){width=80%}

--- 

::: columns
:::: {.column width=70%}

- Algorithm for decimal to binary conversion:

```c
// pseudo-codigo
...

long convert(int n) {
  long bin = 0;
  int rem, i = 1;

  while (n!=0) {
    rem = n % 2;
    n /= 2;
    bin += rem * i;
    i *= 10;
  }

  return bin;
}
...
```

::::
:::: {.column width=30%}


|Decimal|Binário|
|---|---        |
|00 |0000       |
|01 |0001       |
|02 |0010       |
|03 |0011       |
|04 |0100       |
|05 |0101       |
|06 |0110       |
|07 |0111       |
|08 |1000       |
|09 |1001       |
|10 |1010       |
|11 |1011       |
|12 |1100       |
|13 |1101       |
|14 |1110       |
|15 |1111       |


::::
:::

---

### Exemplo
- $255_{10} \rightarrow ?_{2}$
- $22_{10} \rightarrow ?_{2}$
- $533_{10} \rightarrow ?_{2}$


## Binary Arithmetic

::: columns
:::: {.column width=60%}

> Arithmetic circuit units form a part of the CPU. Mathematical  operations include addition, subtraction, multiplication and division.

\

- **Addition**

::::
:::: {.column width=40%}

\centering
![](content/figures/7027bd8cc8cd596a6b6caa9b97a0e6feb2c15aad)

::::
:::

--- 

## Decade Counter Algorithm in hardware

::: columns
:::: {.column width=60%}

> The binary coded decimal (BCD) system provides a convenient way of handling large numbers that need to be input to or output from a PLC.

\centering
![](content/figures/3f0743ffbd1407f98b1207260e25134129da9b51){width=30%}

. . .

![BCD or Decade Counter](content/figures/6cfce214c530c67a46e4725d646c43fc308ac918)

::::
:::: {.column width=40%}

  |Binary     |Dec  | Reset |
  |---        | --- |--     |
  |0000       | 00  |0      |
  |0001       | 01  |0      |
  |0010       | 02  |0      |
  |0011       | 03  |0      |
  |0100       | 04  |0      |
  |0101       | 05  |0      |
  |0110       | 06  |0      |
  |0111       | 07  |0      |
  |1000       | 08  |0      |
  |1001       | 09  |0      |
  |1010       | 00  |1      |
  |0001       | 01  |0      |
  |0010       | 02  |0      |
  |...        |...  |...    |

::::
:::

# Hexadecimal System

> The hexadecimal (hex) numbering system is used in programmable controllers because a word of data consists of 16 data bits, or two 8-bit bytes.


$$ \text{Base}_{16} \rightarrow 0,1,2,3,4,5,6,7,8,9, A, B, C, D, E, F. $$ 

- Example(4) To convert a hexadecimal number $1B7_{16}$ to its decimal equivalent, the hexadecimal digits in the columns are multiplied by the base 16 weight, depending on digit significance:

$$
\begin{split}
  1B7_{16} &= 1\cdot 16^{2}+ 11\cdot 16^1 + 7\cdot 16^0\\
  &= 256\cdot + 176 + 7 \\
  &= 439_{10}
\end{split}
$$

---

### Exercício

Converta de hexadecimal para descimal.

1. $A4_{16}$
1. $FF_{16}$
1. $1D3_{16}$

# Floating Point Arithmetic

> Certain PLC-related computations are performed using floating point arithmetic. The term floating point refers to the fact that the decimal point can float or be placed anywhere relative to the significant digits of the number

- **Single precision**: 32 bits, consisting of Sign bit (1 bit), Exponent (8 bits) and Mantissa (23 bits)

. . .

- **Double precision**: 64 bits, consisting of Sign bit (1 bit), Exponent (11 bits) and Mantissa (52 bits)

\centering
![Decimal number 23.5 represented in a single precisiont floating point format.](content/figures/ec55f1d37610ad5959b59849cf6592f06822d5b3){width=70%}

# Referências

1. BERGER, Hans.**Automating with STEP 7 in STL and SCL: programmable controllers : SIMATIC S7-300/400**. Erlangen: MCD Verlag, 2000. 436 p. : ISBN 3-89578-140-1
1. GEORGINI, Marcelo. **Automação aplicada: descrição e implementação de sistemas seqüenciais com PLCs**. 9. ed. São Paulo, SP: Érica, 2007. 236 p. ISBN 9788571947245.
1. SILVEIRA, Paulo Rogério da; SANTOS, Winderson E. dos. Automação e controle discreto. São Paulo, SP: Érica, 1999. 229 p. ISBN 8571945918.
1. Franchi, Claiton Moro, and Valter Luís Arlindo de CAMARGO. Controladores Lógicos Programáveis Sistemas Discretos. Saraiva Educação SA, 2008.
1. Petruzella, Frank.**Programmable logic controllers**. McGraw-Hill, Inc., 2017.
