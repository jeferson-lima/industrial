---
title: Fundamentals of Logic
author: "Jeferson Lima"
institute: 
    - Department of Electronics (DAELE)
    - Federal Technological University of Paraná (UTFPR)
topic: "industrial-automation-logic"
theme: "Frankfurt"
colortheme: "beaver"
fonttheme: "professionalfonts"
mainfont: "Hack Nerd Font"
fontsize: 10pt
urlcolor: blue
linkstyle: bold
aspectratio: 169
date:
section-titles: false
toc: true
header-includes: |
    \usepackage[american,RPvoltages]{circuitikz}
---


### Licence
This work is licensed under a Creative Commons “AttributionNonCommercial-ShareAlike 4.0 International” license.

### Links
* [UTFPR Moodle](https://moodle.utfpr.edu.br/course/view.php?id=7183)

* password: ******

# Logical Functions

> The PLC, like all digital equipment, operates on the binary principle. The term binary principle refers to the idea that many things can be thought of as existing in only one of two states.

::: columns
:::: {.column width=50%}

\centering
![Car logical AND](content/figures/16602f4da2a76dc0e940072d5374336d4042ed87){width=90%}

::::
:::: {.column width=50%}

. . .

\centering
![Car logical OR](content/figures/aa765cdbb3207fc32e2019b1b6146388af70ff49){width=90%}

::::
:::


# The AND Function
<!---
Step 1
-->
> The symbol drawn in figure below is that of an \textcolor{red}{AND} circuit/truth table/gate.

::: columns
:::: {.column width=60%}

- **AND Electrical Circuit**

\begin{tikzpicture}
\begin{circuitikz}
  \draw (0,0)
   to[battery,v>=$V$] (0,2)   % The voltage source
   to[cute open switch=$s_1$] (2,2)
   to[cute open switch=$s_2$] (4,2)
   to[lamp=$l_1$]   (4,0)
  to[short] (0,0);
\end{circuitikz}   
\end{tikzpicture}

::::
:::: {.column width=40%}

- **AND truth table**

|               |$s_1$   |$s_2$   |$l_1$|
|:---:          |:---:   |:---:   |:---: |
|$\rightarrow$  |Open    |Open    |Off   |
|               |Open    |Close   |Off   |
|               |Close   |Open    |Off   |
|               |Close   |Close   |On    |


::::
:::

---

<!---
Step 2
-->
> The symbol drawn in figure below is that of an \textcolor{red}{AND} circuit/truth table/gate.

::: columns
:::: {.column width=60%}

- **AND Electrical Circuit**

\begin{tikzpicture}
\begin{circuitikz}
  \draw (0,0)
   to[battery,v>=$V$] (0,2)   % The voltage source
   to[cute open switch=$s_1$] (2,2)
   to[cute closed switch=$s_2$] (4,2)
   to[lamp=$l_1$]   (4,0)
  to[short] (0,0);
\end{circuitikz}   
\end{tikzpicture}

::::
:::: {.column width=40%}

- **AND truth table**

|               |$s_1$   |$s_2$   |$l_1$|
|:---:          |:---:   |:---:   |:---: |
|               |Open    |Open    |Off   |
| $\rightarrow$ |Open    |Close   |Off   |
|               |Close   |Open    |Off   |
|               |Close   |Close   |On    |


::::
:::

---

<!---
Step 3
-->
> The symbol drawn in figure below is that of an \textcolor{red}{AND} circuit/truth table/gate.

::: columns
:::: {.column width=60%}

- **AND Electrical Circuit**

\begin{tikzpicture}
\begin{circuitikz}
  \draw (0,0)
   to[battery,v>=$V$] (0,2)   % The voltage source
   to[cute closed switch=$s_1$] (2,2)
   to[cute open switch=$s_2$] (4,2)
   to[lamp=$l_1$]   (4,0)
  to[short] (0,0);
\end{circuitikz}   
\end{tikzpicture}

::::
:::: {.column width=40%}

- **AND truth table**

|               |$s_1$   |$s_2$   |$l_1$|
|:---:          |:---:   |:---:   |:---: |
|               |Open    |Open    |Off   |
|               |Open    |Close   |Off   |
| $\rightarrow$ |Close   |Open    |Off   |
|               |Close   |Close   |On    |


::::
:::

---

<!---
Step 4
-->
> The symbol drawn in figure below is that of an \textcolor{red}{AND} circuit/truth table/gate.

::: columns
:::: {.column width=60%}

- **AND Electrical Circuit**

\begin{tikzpicture}
\begin{circuitikz}
  \draw (0,0)
   to[battery,v>=$V$] (0,2)   % The voltage source
   to[cute closed switch=$s_1$] (2,2)
   to[cute closed switch=$s_2$] (4,2)
   to[lamp=$l_1$, fill=yellow]   (4,0)
  to[short] (0,0);
\end{circuitikz}   
\end{tikzpicture}

::::
:::: {.column width=40%}

- **AND truth table**

|               |$s_1$   |$s_2$   |$l_1$|
|:---:          |:---:   |:---:   |:---: |
|               |Open    |Open    |Off   |
|               |Open    |Close   |Off   |
|               |Close   |Open    |Off   |
| $\rightarrow$ |Close   |Close   |On    |


::::
:::

---

<!---
Step 5
-->
> The symbol drawn in figure below is that of an \textcolor{red}{AND} circuit/truth table/gate.

::: columns
:::: {.column width=40%}

- **Logic Gate**

\

\begin{tikzpicture}
% Circuit style
\ctikzset{
    logic ports=ieee,
    logic ports/scale=0.8,
    logic ports/fill=cyan!50}
% Logic ports
\node[and port] (ORa) at (0,0){};
\draw (ORa.in 1) -- ++(-0.5,0)node[left](In1){$A$};
\draw (ORa.in 2) -- ++(-0.5,0)node[left](In2){$B$};
\draw (ORa.out) -- ++(0.5,0)node[near end,above]{$Y$};
\end{tikzpicture}

::::
:::: {.column width=30%}

**AND truth table**

|$A$     |$B$     |$Y$ |
|:---:   |:---:   |:---: |
|0v      |0v      |0v    |
|0v      |5v      |0v    |
|5v      |0v      |0v    |
|5v      |5v      |5v    |


::::
:::: {.column width=30%}


**Logic truth table**

|$A$  |$B$  |$Y$  |
|:---:|:---:|:---:|
|0    |0    |0    |
|0    |1    |0    |
|1    |0    |0    |
|1    |1    |1    |

::::
:::

---

:::{.block}
### Exercice

- Let's try this
    - https://www.multisim.com

:::


> Multisim Live is a free, online circuit simulator that includes SPICE software, which lets you create, learn and share circuits and electronics online


# The OR Function

<!---
Step 1
-->

> The symbol drawn in figure below is that of an \textcolor{red}{OR gate}.

::: columns
:::: {.column width=60%}

- **OR Electrical Circuit**

\begin{circuitikz}
\draw (0,0)
 to[battery,v>=$V$] (0,3)   % The voltage source
 to[short] (0,3)
 to[short] (1,3)
 to[short] (1,2)
 to[cute open switch=$s_2$] (4,2)
 to[short] (4,3)
 to[short] (5,3)
 to[lamp=$l_1$] (5,0)
 to[short](0,0);
\draw (0,3)
 to[short] (1,3)
 to[short] (1,4)
 to[cute open switch=$s_1$] (4,4)
 to[short] (4,2);
\end{circuitikz}

::::
:::: {.column width=40%}

- **OR truth table**

|               |$s_1$   |$s_2$   |$l_1$|
|:---:          |:---:   |:---:   |:---: |
|$\rightarrow$  |Open    |Open    |Off   |
|               |Open    |Close   |On    |
|               |Close   |Open    |On    |
|               |Close   |Close   |On    |

::::
:::

---

<!---
Step 2
-->

> The symbol drawn in figure below is that of an \textcolor{red}{OR gate}.

::: columns
:::: {.column width=60%}

- **OR Electrical Circuit**

\begin{circuitikz}
\draw (0,0)
 to[battery,v>=$V$] (0,3)   % The voltage source
 to[short] (0,3)
 to[short] (1,3)
 to[short] (1,2)
 to[cute closed switch=$s_2$] (4,2)
 to[short] (4,3)
 to[short] (5,3)
 to[lamp=$l_1$, fill=yellow] (5,0)
 to[short](0,0);
\draw (0,3)
 to[short] (1,3)
 to[short] (1,4)
 to[cute open switch=$s_1$] (4,4)
 to[short] (4,2);
\end{circuitikz}

::::
:::: {.column width=40%}

- **OR truth table**

|               |$s_1$   |$s_2$   |$l_1$|
|:---:          |:---:   |:---:   |:---: |
|               |Open    |Open    |Off   |
| $\rightarrow$ |Open    |Close   |On    |
|               |Close   |Open    |On    |
|               |Close   |Close   |On    |

::::
:::

---

<!---
Step 3
-->

> The symbol drawn in figure below is that of an \textcolor{red}{OR gate}.

::: columns
:::: {.column width=60%}

- **OR Electrical Circuit**

\begin{circuitikz}
\draw (0,0)
 to[battery,v>=$V$] (0,3)   % The voltage source
 to[short] (0,3)
 to[short] (1,3)
 to[short] (1,2)
 to[cute open switch=$s_2$] (4,2)
 to[short] (4,3)
 to[short] (5,3)
 to[lamp=$l_1$, fill=yellow] (5,0)
 to[short](0,0);
\draw (0,3)
 to[short] (1,3)
 to[short] (1,4)
 to[cute closed switch=$s_1$] (4,4)
 to[short] (4,2);
\end{circuitikz}

::::
:::: {.column width=40%}

- **OR truth table**

|               |$s_1$   |$s_2$   |$l_1$|
|:---:          |:---:   |:---:   |:---: |
|               |Open    |Open    |Off   |
| $\rightarrow$ |Open    |Close   |On    |
|               |Close   |Open    |On    |
|               |Close   |Close   |On    |

::::
:::

---

<!---
Step 4
-->

> The symbol drawn in figure below is that of an \textcolor{red}{OR gate}.

::: columns
:::: {.column width=60%}

- **OR Electrical Circuit**

\begin{circuitikz}
\draw (0,0)
 to[battery,v>=$V$] (0,3)   % The voltage source
 to[short] (0,3)
 to[short] (1,3)
 to[short] (1,2)
 to[cute closed switch=$s_2$] (4,2)
 to[short] (4,3)
 to[short] (5,3)
 to[lamp=$l_1$, fill=yellow] (5,0)
 to[short](0,0);
\draw (0,3)
 to[short] (1,3)
 to[short] (1,4)
 to[cute closed switch=$s_1$] (4,4)
 to[short] (4,2);
\end{circuitikz}

::::
:::: {.column width=40%}

- **OR truth table**

|               |$s_1$   |$s_2$   |$l_1$|
|:---:          |:---:   |:---:   |:---: |
|               |Open    |Open    |Off   |
|               |Open    |Close   |Off   |
|               |Close   |Open    |Off   |
| $\rightarrow$ |Close   |Close   |On    |

::::
:::

---

<!---
Step 5
-->
> The symbol drawn in figure below is that of an \textcolor{red}{OR gate}.

::: columns
:::: {.column width=40%}

- **Logic Gate**

\

\begin{tikzpicture}
% Circuit style
\ctikzset{
    logic ports=ieee,
    logic ports/scale=0.8,
    logic ports/fill=cyan!50}
% Logic ports
\node[or port] (ORa) at (0,0){};
\draw (ORa.in 1) -- ++(-0.5,0)node[left](In1){$A$};
\draw (ORa.in 2) -- ++(-0.5,0)node[left](In2){$B$};
\draw (ORa.out) -- ++(0.5,0)node[near end,above]{$Y$};
\end{tikzpicture}

::::
:::: {.column width=30%}

**AND truth table**

|$A$     |$B$     |$Y$ |
|:---:   |:---:   |:---: |
|0v      |0v      |0v    |
|0v      |5v      |5v    |
|5v      |0v      |5v    |
|5v      |5v      |5v    |


::::
:::: {.column width=30%}


**Logic truth table**

|$A$  |$B$  |$Y$  |
|:---:|:---:|:---:|
|0    |0    |0    |
|0    |1    |1    |
|1    |0    |1    |
|1    |1    |1    |

::::
:::


# The NOT Function

> The symbol drawn in figure below is that of an NOT circuit/truth table/gate.

::: columns
:::: {.column width=40%}
\

![](content/figures/6c44b4eb7d56cf59fbd60a99cae23108e61cb593)


::::
:::: {.column width=30%}

|SW-A     |Light |
|:---:    |:---: |
|No Push  |On    |
|Push     |Off   |

_the AND truth table_

. . .

::::
:::: {.column width=30%}

\begin{tikzpicture}
% Circuit style
\ctikzset{
    logic ports=ieee,
    logic ports/scale=0.8,
    logic ports/fill=cyan!50}
% Logic ports
\node[not port] (ORa) at (0,0){};
\draw (ORa.in) -- ++(-0.5,0)node[left](In1){SW-A};
\draw (ORa.out) -- ++(0.5,0)node[near end,above]{Light};
\end{tikzpicture}

. . .

|SW-A |SW-B |Light|
|:---:|:---:|:---:|
|0    |0    |0    |
|0    |1    |0    |
|1    |0    |0    |
|1    |1    |1    |

::::
:::

> _Unlike the AND and OR functions, the NOT function can have only one input._

---

# The Exclusive-OR Function

- An often-used combination of gates is the exclusive-OR (XOR) function. 

::: columns
:::: {.column width=40%}
\


::::
:::: {.column width=30%}

|SW-A    |SW-B    |Light |
|:---:   |:---:   |:---: |
|Open    |Open    |Off   |
|Open    |Close   |On    |
|Close   |Open    |On    |
|Close   |Close   |Off   |

_the XOR truth table_

. . .

::::
:::: {.column width=30%}

\begin{tikzpicture}
% Circuit style
\ctikzset{
    logic ports=ieee,
    logic ports/scale=0.8,
    logic ports/fill=cyan!50}
% Logic ports
\node[xor port] (ORa) at (0,0){};
\draw (ORa.in 1) -- ++(-0.5,0)node[left](In1){SW-A};
\draw (ORa.in 2) -- ++(-0.5,0)node[left](In2){SW-B};
\draw (ORa.out) -- ++(0.5,0)node[near end,above]{Light};
\end{tikzpicture}

. . .

|SW-A |SW-B |Light|
|:---:|:---:|:---:|
|0    |0    |0    |
|0    |1    |1    |
|1    |0    |1    |
|1    |1    |0    |

::::
:::

---

# Boolean Algebra

> The mathematical study of the binary number system and logic is called Boolean algebra. 

> The purpose of this algebra is to provide a simple way of writing complicated combinations of logic statements. There are many applications where Boolean algebra could be applied to solving PLC programming problems

---

- Logical operators used singly to form logical statements.

::: columns
:::: {.column width=33%}

\begin{tikzpicture}
% Circuit style
\ctikzset{
    logic ports=ieee,
    logic ports/scale=0.8,
    logic ports/fill=cyan!50}
% Logic ports
\node[and port] (ORa) at (0,0){};
\draw (ORa.in 1) -- ++(-0.5,0)node[left](In1){A};
\draw (ORa.in 2) -- ++(-0.5,0)node[left](In2){B};
\draw (ORa.out) -- ++(0.5,0)node[near end,above]{Light};
\end{tikzpicture}

::::
:::: {.column width=33%}

\begin{tikzpicture}
% Circuit style
\ctikzset{
    logic ports=ieee,
    logic ports/scale=0.8,
    logic ports/fill=cyan!50}
% Logic ports
\node[and port] (ORa) at (0,0){};
\draw (ORa.in 1) -- ++(-0.5,0)node[left](In1){SW-A};
\draw (ORa.in 2) -- ++(-0.5,0)node[left](In2){SW-B};
\draw (ORa.out) -- ++(0.5,0)node[near end,above]{Light};
\end{tikzpicture}

::::
:::: {.column width=33%}

\begin{tikzpicture}
% Circuit style
\ctikzset{
    logic ports=ieee,
    logic ports/scale=0.8,
    logic ports/fill=cyan!50}
% Logic ports
\node[and port] (ORa) at (0,0){};
\draw (ORa.in 1) -- ++(-0.5,0)node[left](In1){SW-A};
\draw (ORa.in 2) -- ++(-0.5,0)node[left](In2){SW-B};
\draw (ORa.out) -- ++(0.5,0)node[near end,above]{Light};
\end{tikzpicture}

::::
:::

---

\begin{tikzpicture}
% Circuit style
\ctikzset{
    logic ports=ieee,
    logic ports/scale=0.8,
    logic ports/fill=cyan!50}
% Logic ports
\node[or port] (ORa) at (0,0){};
\node[not port] (Noa) at (0,-2){};
\node[or port] (ORb) at (0,-4){};
\node[not port] (Nob) at (2.5,0){};
\node[and port] (ANDa) at (2.5,-3){};
\node[or port] (ORc) at (5,-1.5){};
% Connection
\draw (ORa.out) -- (Nob.in);
\draw (Noa.out) -| (ANDa.in 1);
\draw (ORb.out) -| (ANDa.in 2);
\draw (ANDa.out) -|  (ORc.in 2);
\draw (Nob.out) -| (ORc.in 1);
\draw (ORc.out) -- ++(1,0) node[near end,above]{Y};
\draw (ORa.in 1) -- ++(-1.5,0)node[left](In1){A};
\draw (ORb.in 2) -- ++(-1.5,0)node[left](In3){C};
% Jump crossing element
\node at (ORa.in 2)
[    below,
    jump crossing,
    rotate=-90,
    scale=1.3
](X){};
\draw (Noa.in) -| (X.east)
    (X.west) to[short,-*] (X.west |- ORa.in 1); 
\draw ($ (In1) !.5! (In3) $) node[]{B}
    ++ (0.4,0) to[short,-*] ++(0.5,0) coordinate(a)
    |- (X.south) (a) |- (ORb.in 1);
\end{tikzpicture}



# References

1. BERGER, Hans.**Automating with STEP 7 in STL and SCL: programmable controllers : SIMATIC S7-300/400**. Erlangen: MCD Verlag, 2000. 436 p. : ISBN 3-89578-140-1
1. GEORGINI, Marcelo. **Automação aplicada: descrição e implementação de sistemas seqüenciais com PLCs**. 9. ed. São Paulo, SP: Érica, 2007. 236 p. ISBN 9788571947245.
1. SILVEIRA, Paulo Rogério da; SANTOS, Winderson E. dos. **Automação e controle discreto**. São Paulo, SP: Érica, 1999. 229 p. ISBN 8571945918.
1. Franchi, Claiton Moro, and Valter Luís Arlindo de CAMARGO. **Controladores Lógicos Programáveis Sistemas Discretos**. Saraiva Educação SA, 2008.
1. Petruzella, Frank.**Programmable logic controllers**. McGraw-Hill, Inc., 2017.
