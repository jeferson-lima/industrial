# Industrial Automation Course

[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/)


(WIP)

## Sumary
1. **Industrial Automation**
    1. [A Brief History of Manufacturing Application](https://jeferson-lima.gitlab.io/industrial/1-introduction.pdf)
    1. [Modern Robotics](https://jeferson-lima.gitlab.io/mobile-robotics/modern-robots-aula.pdf)
1. **Numerical System and Algorithms**
    1. [Numerical System](https://jeferson-lima.gitlab.io/industrial/2-numerical-system.pdf)
    1. [Fundamentals of Logic](https://jeferson-lima.gitlab.io/industrial/3-fund-of-logic.pdf)
1. **Industrial Instrumentation**
    1. [Techniques and Instrumentation]()
    1. [Instrumentation Symbology]()
    1. [Industrial Newworking]()

## References

